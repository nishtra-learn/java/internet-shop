package com.example.internetshop.models;

public class AuthInfo {
    public boolean isAuthenticated;
    public String username;
    public boolean isAdmin;

    public boolean isUserRoleAdmin(String role) {
        return role.equals("ADMIN");
    }
}
