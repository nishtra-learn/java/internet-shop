package com.example.internetshop.models;

import com.example.internetshop.models.domain.Product;
import com.sun.istack.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Scope("session")
public class ShoppingCart {
    private List<CartItem> products = new ArrayList<>();

    public List<CartItem> getItems() {
        return products;
    }


    public void addItem(@NotNull Product product) {
        Optional<CartItem> matchedItem = products.stream()
                .filter(i -> i.getProduct().getId() == product.getId())
                .findFirst();
        if (matchedItem.isPresent()) {
            matchedItem.get().increaseAmount();
        }
        else {
            products.add(new CartItem(product));
        }
    }

    public void removeItem(Long productId) {
        products = products.stream()
                .filter(i -> i.getProduct().getId() != productId)
                .collect(Collectors.toList());
    }

    public void increaseItemAmount(Long productId) {
        Optional<CartItem> matchedItem = products.stream()
                .filter(i -> i.getProduct().getId() == productId)
                .findFirst();
        matchedItem.ifPresent(CartItem::increaseAmount);
    }

    public void decreaseItemAmount(Long productId) {
        Optional<CartItem> matchedItem = products.stream()
                .filter(i -> i.getProduct().getId() == productId)
                .findFirst();
        matchedItem.ifPresent(CartItem::decreaseAmount);
    }

    public void clear() {
        this.products.clear();
    }
}
