package com.example.internetshop.models;

import com.example.internetshop.models.domain.Product;

public class CartItem {
    private Product product;
    private int quantity;

    public CartItem() {
    }

    public CartItem(Product product) {
        this.product = product;
        this.quantity = 1;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity)
            throws IllegalArgumentException
    {
        if (quantity < 1) {
            throw new IllegalArgumentException("Can't be negative amount of items");
        }

        this.quantity = quantity;
    }

    public void increaseAmount() {
        this.quantity++;
    }

    public void decreaseAmount() {
        if (this.quantity > 1) {
            this.quantity--;
        }
    }
}
