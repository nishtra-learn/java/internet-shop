package com.example.internetshop.models.projection;

public class UserMoneySpent {
    private String username;
    private Double moneySpent;

    public UserMoneySpent() {
    }

    public UserMoneySpent(String username, Double moneySpent) {
        this.username = username;
        this.moneySpent = moneySpent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Double getMoneySpent() {
        return moneySpent;
    }

    public void setMoneySpent(Double moneySpent) {
        this.moneySpent = moneySpent;
    }
}
