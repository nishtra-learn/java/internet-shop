package com.example.internetshop.models.projection;

public class ProductPopularity {
    private Long productId;
    private String productName;
    private Long timesOrdered;

    public ProductPopularity() {
    }

    public ProductPopularity(Long productId, String productName, Long timesOrdered) {
        this.productId = productId;
        this.productName = productName;
        this.timesOrdered = timesOrdered;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getTimesOrdered() {
        return timesOrdered;
    }

    public void setTimesOrdered(Long timesOrdered) {
        this.timesOrdered = timesOrdered;
    }
}
