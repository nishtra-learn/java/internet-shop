package com.example.internetshop.models.projection;

public class UserOrdersMade {
    private String username;
    private Long numberOfOrders;

    public UserOrdersMade() {
    }

    public UserOrdersMade(String username, Long numberOfOrders) {
        this.username = username;
        this.numberOfOrders = numberOfOrders;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getNumberOfOrders() {
        return numberOfOrders;
    }

    public void setNumberOfOrders(Long numberOfOrders) {
        this.numberOfOrders = numberOfOrders;
    }
}
