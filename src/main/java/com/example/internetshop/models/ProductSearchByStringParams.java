package com.example.internetshop.models;

import org.springframework.web.bind.annotation.RequestParam;

public class ProductSearchByStringParams {
    private boolean searchInName = true;
    private boolean searchInBrand = false;
    private boolean searchInCategory = false;

    public boolean isSearchInName() {
        return searchInName;
    }

    public void setSearchInName(boolean searchInName) {
        this.searchInName = searchInName;
    }

    public boolean isSearchInBrand() {
        return searchInBrand;
    }

    public void setSearchInBrand(boolean searchInBrand) {
        this.searchInBrand = searchInBrand;
    }

    public boolean isSearchInCategory() {
        return searchInCategory;
    }

    public void setSearchInCategory(boolean searchInCategory) {
        this.searchInCategory = searchInCategory;
    }
}
