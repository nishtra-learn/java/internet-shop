package com.example.internetshop.models;

import java.util.ArrayList;
import java.util.List;

public class ProductSearchFilters {
    private List<Long> categories = new ArrayList<>();
    private List<String> brands = new ArrayList<>();
    private double minPrice = 0;
    private double maxPrice = 100;
    private double priceRangeMin = -1;
    private double priceRangeMax = -1;


    public boolean isAnyFiltersApplied() {

        return categories.size() > 0 || brands.size() > 0 || isPriceSet();
    }

    public boolean isPriceSet() {
        return (priceRangeMin > -1 && minPrice > priceRangeMin) || (priceRangeMax > -1 && maxPrice < priceRangeMax);
    }


    //region Getters/Setters
    public List<Long> getCategories() {
        return categories;
    }

    public void setCategories(List<Long> categories) {
        this.categories = categories;
    }

    public List<String> getBrands() {
        return brands;
    }

    public void setBrands(List<String> brands) {
        this.brands = brands;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public double getPriceRangeMin() {
        return priceRangeMin;
    }

    public void setPriceRangeMin(double priceRangeMin) {
        this.priceRangeMin = priceRangeMin;
    }

    public double getPriceRangeMax() {
        return priceRangeMax;
    }

    public void setPriceRangeMax(double priceRangeMax) {
        this.priceRangeMax = priceRangeMax;
    }
    //endregion
}
