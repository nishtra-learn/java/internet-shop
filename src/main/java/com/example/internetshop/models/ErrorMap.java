package com.example.internetshop.models;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ErrorMap {
    private Map<String, List<String>> errorMap = new HashMap<>();

    public Map<String, List<String>> getErrorMap() {
        return errorMap;
    }

    public void setErrorMap(Map<String, List<String>> errorMap) {
        this.errorMap = errorMap;
    }

    public void addError(String field, String message) {
        if (errorMap.containsKey(field)) {
            errorMap.get(field).add(message);
        }
        else {
            errorMap.put(field, List.of(message));
        }
    }

    public List<String> getFieldErrors(String field) {
        if (errorMap.containsKey(field)) {
            return errorMap.get(field);
        }
        else {
            return new ArrayList<>();
        }
    }

    public void setFiledError(String field, List<String> errors) {
        errorMap.put(field, errors);
    }

    public boolean fieldHasErrors(String field) {
        return errorMap.containsKey(field);
    }

    public static ErrorMap fromBindingResult(BindingResult bindingResult) {
        var errors = new ErrorMap();
        for (var err : bindingResult.getAllErrors()) {
            if (err instanceof FieldError) {
                var fieldError = (FieldError) err;
                var field = fieldError.getField();
                var msg = fieldError.getDefaultMessage();
                errors.addError(field, msg);
            }
        }
        return errors;
    }
}
