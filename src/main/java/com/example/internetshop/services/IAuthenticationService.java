package com.example.internetshop.services;

import com.example.internetshop.models.domain.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

public interface IAuthenticationService {
    Authentication getAuthentication();
    boolean isAuthenticated();
    UserDetails getUserDetails();
    String getName();
    User getAuthenticatedUserFromDatabase();
    boolean isAdmin();
}