package com.example.internetshop.services;

import com.example.internetshop.models.domain.Order;
import com.example.internetshop.models.domain.OrderItem;
import com.example.internetshop.models.ShoppingCart;
import com.example.internetshop.repositories.OrderRepository;
import com.example.internetshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.security.sasl.AuthenticationException;
import javax.transaction.Transactional;

@Service
@Scope("session")
public class ShippingOrderService {
    private final ShoppingCart cart;
    private final IAuthenticationService authProvider;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public ShippingOrderService(
            ShoppingCart cart,
            IAuthenticationService authProvider,
            UserRepository userRepository,
            OrderRepository orderRepository)
    {
        this.cart = cart;
        this.authProvider = authProvider;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
    }

    @Transactional
    public void makeOrder() throws AuthenticationException {
        String username = authProvider.getName();

        if (username == null) {
            throw new AuthenticationException("Can't make an order with not authenticated user");
        }

        var user = userRepository.findByUsername(username);
        if (user == null) {
            throw new AuthenticationException("Can't make an order with not registered user");
        }

        var order = new Order(user);
        for (var item : cart.getItems()) {
            order.getItems().add(new OrderItem(order, item));
        }
        orderRepository.save(order);
    }
}
