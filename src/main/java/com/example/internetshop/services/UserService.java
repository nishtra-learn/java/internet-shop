package com.example.internetshop.services;

import com.example.internetshop.models.domain.User;
import com.example.internetshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.security.sasl.AuthenticationException;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final IAuthenticationService authService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(
            UserRepository userRepository,
            IAuthenticationService authService,
            PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.authService = authService;
        this.passwordEncoder = passwordEncoder;
    }

    public void updateUserWithAuthorizationCheck(User user)
            throws AuthenticationException, UsernameNotFoundException
    {
        var currentUser = authService.getAuthenticatedUserFromDatabase();
        performAuthorizationCheck(user, currentUser);

        // check that a user with provided id exists in the database
        var userDbEntry = userRepository
                .findById(user.getId())
                .orElseThrow(() ->
                        new UsernameNotFoundException("User with this id doesn't exist in the database"));

        // restore fields not provided by the front from db entry
        user.setPassword(userDbEntry.getPassword());
        user.setRole(userDbEntry.getRole());

        userRepository.save(user);
    }

    public void updateUserPasswordWithAuthorizationCheck(Long userId, String newPassword)
            throws AuthenticationException, UsernameNotFoundException
    {
        // check that a user with provided id exists in the database
        var userDbEntry = userRepository
                .findById(userId)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User with this id doesn't exist in the database"));

        var currentUser = authService.getAuthenticatedUserFromDatabase();
        performAuthorizationCheck(userDbEntry, currentUser);

        userDbEntry.setPassword(passwordEncoder.encode(newPassword));

        userRepository.save(userDbEntry);
    }

    private void performAuthorizationCheck(User userToUpdate, User authenticatedUser)
            throws AuthenticationException
    {
        var isAdmin = authService.isAdmin();
        if (authenticatedUser == null) {
            throw new AuthenticationException("Unauthenticated user");
        }
        if ((authenticatedUser.getId() != userToUpdate.getId()) && !isAdmin) {
            throw new AuthenticationException("Non-admin users are allowed to change only their own profile");
        }
    }

    public void banUser(Long userId) {
        var userOpt = userRepository.findById(userId);
        if (userOpt.isEmpty()) return;

        var user = userOpt.get();
        if (user.getRole().equals("ADMIN")) {
            throw new IllegalStateException("Can not ban an admin");
        }

        user.setActive(false);
        userRepository.save(user);
    }

    public void unbanUser(Long userId) {
        var userOpt = userRepository.findById(userId);
        if (userOpt.isEmpty()) return;

        var user = userOpt.get();
        user.setActive(true);
        userRepository.save(user);
    }

    public void giveAdminRights(Long userId) {
        var userOpt = userRepository.findById(userId);
        if (userOpt.isEmpty()) return;

        var user = userOpt.get();
        if (!user.isActive()) {
            throw new IllegalStateException("Can not give admin rights to a banned user");
        }

        user.setRole("ADMIN");
        userRepository.save(user);
    }

    public void revokeAdminRights(Long userId) {
        var userOpt = userRepository.findById(userId);
        if (userOpt.isEmpty()) return;

        var user = userOpt.get();
        user.setRole("USER");
        userRepository.save(user);
    }
}
