package com.example.internetshop.services;

import com.example.internetshop.models.domain.User;
import com.example.internetshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    final UserRepository userRepo;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException
    {
        User matchedUser = userRepo.findByUsername(username);
        if (matchedUser == null) {
            throw new UsernameNotFoundException("Unregistered user: " + username);
        }

        UserDetails userDetails = org.springframework.security.core.userdetails.User
                .builder()
                .username(matchedUser.getUsername())
                .password(matchedUser.getPassword())
                .roles(matchedUser.getRole())
                .disabled(!matchedUser.isActive())
                .build();
        return userDetails;
    }
}
