package com.example.internetshop.services;

import com.example.internetshop.models.domain.ProductCategory;

import java.util.List;
import java.util.Optional;

public interface IProductCategoryService {
    ProductCategory save(ProductCategory productCategory);

    Optional<ProductCategory> findById(Long id);

    List<ProductCategory> findAll();

    void deleteById(Long id);

    void delete(ProductCategory productCategory);

    List<ProductCategory> getAllOrderedByName();
}
