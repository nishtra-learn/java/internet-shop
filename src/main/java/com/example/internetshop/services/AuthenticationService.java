package com.example.internetshop.services;

import com.example.internetshop.models.domain.User;
import com.example.internetshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService implements IAuthenticationService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public boolean isAuthenticated() {
        var isAuthenticated = false;
        var auth = getAuthentication();
        if (auth.isAuthenticated() && !(auth instanceof AnonymousAuthenticationToken)) {
            isAuthenticated = true;
        }
        return isAuthenticated;
    }

    @Override
    public UserDetails getUserDetails() {
        var auth = getAuthentication();
        org.springframework.security.core.userdetails.User userDetails = null;
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            userDetails = (org.springframework.security.core.userdetails.User) auth.getPrincipal();
        }
        return userDetails;
    }

    @Override
    public String getName() {
        var authentication = getAuthentication();
        String username = null;
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            username = authentication.getName();
        }
        return username;
    }

    @Override
    public User getAuthenticatedUserFromDatabase() {
        var username = getName();
        var user = userRepository.findByUsername(username);
        return user;
    }

    @Override
    public boolean isAdmin() {
        var userDetails = getUserDetails();
        if (userDetails == null) return false;

        var isAdmin = userDetails.getAuthorities()
                .stream()
                .anyMatch(a -> a.getAuthority().equalsIgnoreCase("ROLE_ADMIN"));
        return isAdmin;
    }
}
