package com.example.internetshop.services;

import com.example.internetshop.models.domain.ProductCategory;
import com.example.internetshop.repositories.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductCategoryService implements IProductCategoryService {
    @Autowired
    ProductCategoryRepository categoryRepository;


    @Override
    public ProductCategory save(ProductCategory productCategory) {
        return categoryRepository.save(productCategory);
    }

    @Override
    public Optional<ProductCategory> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public List<ProductCategory> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        var category = categoryRepository.findById(id);
        if (category.isPresent()) {
            delete(category.get());
        }
    }

    @Override
    public void delete(ProductCategory productCategory) {
        for (var product : productCategory.getProducts()) {
            product.setCategory(null);
        }
        categoryRepository.delete(productCategory);
    }

    @Override
    public List<ProductCategory> getAllOrderedByName() {
        return categoryRepository.getAllOrderedByName();
    }
}
