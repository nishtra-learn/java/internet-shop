package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.models.ErrorMap;
import com.example.internetshop.models.domain.Product;
import com.example.internetshop.repositories.ProductRepository;
import com.example.internetshop.services.IAuthenticationService;
import com.example.internetshop.services.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/products")
public class ProductController implements IBasicModelAttrProvider {
    private final IAuthenticationService authService;
    private final ProductRepository productRepository;
    private final ProductCategoryService categoryService;

    @Autowired
    public ProductController(
            IAuthenticationService authService,
            ProductRepository productRepository,
            ProductCategoryService categoryService)
    {
        this.authService = authService;
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    @GetMapping("")
    public String index(Model model) {
        var products = productRepository.findAll();
        model.addAttribute("products", products);
        return "admin/products";
    }

    @GetMapping("new")
    public String create(Model model) {
        var product = new Product();
        addCategoryListToModel(model);
        model.addAttribute("product", product);
        model.addAttribute("action", "create");
        return "admin/product_form";
    }

    @PostMapping("new")
    public String createConfirm(
            @ModelAttribute Product product,
            BindingResult result,
            Long categoryId,
            Model model)
    {
        if (categoryId != -1) {
            var cat = categoryService.findById(categoryId);
            product.setCategory(cat.orElse(null));
        }
        if (result.hasErrors()) {
            var errors = ErrorMap.fromBindingResult(result);
            addCategoryListToModel(model);
            model.addAttribute("errors", errors);
            model.addAttribute("action", "create");
            return "admin/product_form";
        }

        productRepository.save(product);
        return "redirect:/products";
    }

    @GetMapping("update")
    public String update(
            @RequestParam Long id,
            Model model)
    {
        var product = productRepository.findById(id);

        if (product.isEmpty()) {
            return "redirect:/products";
        }

        addCategoryListToModel(model);
        model.addAttribute("product", product.get());
        model.addAttribute("action", "update");
        return "admin/product_form";
    }

    @PostMapping("update")
    public String updateConfirm(
            @ModelAttribute Product product,
            BindingResult result,
            Long categoryId,
            Model model)
    {
        if (categoryId != -1) {
            var cat = categoryService.findById(categoryId);
            product.setCategory(cat.orElse(null));
        }
        if (result.hasErrors()) {
            var errors = ErrorMap.fromBindingResult(result);
            addCategoryListToModel(model);
            model.addAttribute("errors", errors);
            model.addAttribute("action", "update");
            return "admin/product_form";
        }

        productRepository.save(product);
        return "redirect:/products";
    }

    @PostMapping("delete")
    public String deleteConfirm(@RequestParam Long id) {
        productRepository.deleteById(id);
        return "redirect:/products";
    }

    private void addCategoryListToModel(Model model) {
        var categories = categoryService.getAllOrderedByName();
        model.addAttribute("categories", categories);
    }


    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
