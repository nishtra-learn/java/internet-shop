package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.models.domain.Order;
import com.example.internetshop.repositories.OrderRepository;
import com.example.internetshop.repositories.UserRepository;
import com.example.internetshop.services.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/orders")
@Secured({"ROLE_USER", "ROLE_ADMIN"})
public class OrderController implements IBasicModelAttrProvider {
    private final IAuthenticationService authService;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final IAuthenticationService authProvider;

    @Autowired
    public OrderController(
            IAuthenticationService authService,
            UserRepository userRepository,
            OrderRepository orderRepository,
            IAuthenticationService authProvider)
    {
        this.authService = authService;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
        this.authProvider = authProvider;
    }

    @GetMapping("")
    public String index(@RequestParam(required = false) Long id, Model model)
    {
        var username = authProvider.getName();
        var orderList = orderRepository.findByUserUsername(username);
        Order selectedOrder = id != null
                ? orderRepository.findById(id).orElse(null)
                : null;

        model.addAttribute("orderList", orderList);
        model.addAttribute("order", selectedOrder);
        return "order_page";
    }


    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
