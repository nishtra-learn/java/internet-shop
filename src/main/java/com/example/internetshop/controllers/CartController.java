package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.models.CartItem;
import com.example.internetshop.models.domain.Product;
import com.example.internetshop.models.ShoppingCart;
import com.example.internetshop.repositories.ProductRepository;
import com.example.internetshop.repositories.UserRepository;
import com.example.internetshop.services.IAuthenticationService;
import com.example.internetshop.services.ShippingOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.security.sasl.AuthenticationException;
import java.util.List;

@Controller
@Scope("request")
@RequestMapping(value = "/cart")
public class CartController implements IBasicModelAttrProvider {
    private final IAuthenticationService authService;
    private final ShoppingCart cart;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final ShippingOrderService orderService;

    @Autowired
    public CartController(
            IAuthenticationService authService,
            ShoppingCart cart,
            ProductRepository productRepository,
            UserRepository userRepository,
            ShippingOrderService orderService)
    {
        this.authService = authService;
        this.cart = cart;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.orderService = orderService;
    }


    @GetMapping(value = "")
    public String index(Model model) {
        List<CartItem> cartItems = cart.getItems();
        double total = cartItems.stream()
                .mapToDouble(i -> i.getProduct().getPrice() * i.getQuantity())
                .sum();
        model.addAttribute("total", total);
        model.addAttribute("cartItems", cartItems);
        return "cart";
    }

    @GetMapping("add")
    public String addItem(@RequestParam Long productid) {
        Product product = productRepository.findById(productid).orElse(null);
        if (product != null) {
            cart.addItem(product);
        }
        return "redirect:/cart";
    }

    @GetMapping("increase")
    public String increaseItemAmount(@RequestParam Long productid) {
        cart.increaseItemAmount(productid);
        return "redirect:/cart";
    }

    @GetMapping("decrease")
    public String decreaseItemAmount(@RequestParam Long productid) {
        cart.decreaseItemAmount(productid);
        return "redirect:/cart";
    }

    @PostMapping("remove")
    public String removeItem(@RequestParam Long productid) {
        cart.removeItem(productid);
        return "redirect:/cart";
    }

    @GetMapping("order")
    public String makeOrder()
            throws AuthenticationException
    {
        orderService.makeOrder();
        cart.clear();

        return "redirect:/";
    }


    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
