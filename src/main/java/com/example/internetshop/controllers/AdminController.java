package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.repositories.ProductRepository;
import com.example.internetshop.repositories.UserRepository;
import com.example.internetshop.services.IAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/admin")
public class AdminController implements IBasicModelAttrProvider {
    private final IAuthenticationService authService;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;

    @Autowired
    public AdminController(
            IAuthenticationService authService,
            ProductRepository productRepository,
            UserRepository userRepository)
    {
        this.authService = authService;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("")
    public String index() {
        return "admin/admin_panel";
    }

    @GetMapping("report/top-products")
    public String reportTopProducts(Model model) {
        var topProducts = productRepository.findTop100ProductOrderByPopularity();
        topProducts = topProducts.stream().limit(5).collect(Collectors.toList());
        model.addAttribute("topProducts", topProducts);
        return "admin/top_products";
    }

    @GetMapping("report/top-customers/money-spent")
    public String reportTopCustomersByMoneySpent(Model model) {
        var topCustomers = userRepository.findTop100UsersOrderByMoneySpent();
        topCustomers = topCustomers.stream().limit(5).collect(Collectors.toList());
        model.addAttribute("topCustomers", topCustomers);
        return "admin/top_customers_by_money";
    }

    @GetMapping("report/top-customers/orders-made")
    public String reportTopCustomersByOrdersMade(Model model) {
        var topCustomers = userRepository.findTop100UsersOrderByOrdersMade();
        topCustomers = topCustomers.stream().limit(5).collect(Collectors.toList());
        model.addAttribute("topCustomers", topCustomers);
        return "admin/top_customers_by_orders";
    }


    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
