package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.controllers.model_attribute_providers.ISearchFiltersModelAttrProvider;
import com.example.internetshop.models.domain.Product;
import com.example.internetshop.repositories.ProductRepository;
import com.example.internetshop.services.IAuthenticationService;
import com.example.internetshop.services.IProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class HomeController
        implements IBasicModelAttrProvider,
        ISearchFiltersModelAttrProvider {
    private final IAuthenticationService authService;
    private final ProductRepository productRepository;
    private final IProductCategoryService categoryService;

    @Autowired
    public HomeController(
            IAuthenticationService authService,
            ProductRepository productRepository,
            IProductCategoryService categoryService)
    {
        this.authService = authService;
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    //@GetMapping(value = {"", "/"})
    @RequestMapping(path = {"", "/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String index(Authentication auth, Model model) {
        List<Product> products = productRepository.findAll();
        model.addAttribute("products", products);
        return "index";
    }

    @ModelAttribute
    public void getBrandsAndCategories(Model model) {
        var categories = categoryService.getAllOrderedByName();
        var brands = productRepository.findDistinctBrandNames();
        model.addAttribute("productCategories", categories);
        model.addAttribute("productBrands", brands);
    }


    @Override
    public ProductRepository getProductRepository() {
        return productRepository;
    }

    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
