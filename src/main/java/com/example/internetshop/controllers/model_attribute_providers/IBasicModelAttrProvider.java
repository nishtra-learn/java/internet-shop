package com.example.internetshop.controllers.model_attribute_providers;

/**
 * Combines IAuthDataModelAttrProvider and ISearchByStringParamsModelAttrProvider interfaces
 * */
public interface IBasicModelAttrProvider
        extends IAuthDataModelAttrProvider, ISearchByStringParamsModelAttrProvider{
}
