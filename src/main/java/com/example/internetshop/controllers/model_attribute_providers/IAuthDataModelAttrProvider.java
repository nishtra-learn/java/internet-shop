package com.example.internetshop.controllers.model_attribute_providers;

import com.example.internetshop.models.AuthInfo;
import com.example.internetshop.services.IAuthenticationService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Provides authentication data to model. Should be implemented by Controller.
 */
public interface IAuthDataModelAttrProvider {
    IAuthenticationService getAuthService();

    @ModelAttribute
    default void setAuthAttributes(Model model) {
        var authService = getAuthService();

        var auth = new AuthInfo();
        auth.isAuthenticated = authService.isAuthenticated();
        auth.username = authService.getName();
        auth.isAdmin = authService.isAdmin();

        model.addAttribute("auth", auth);
    }
}
