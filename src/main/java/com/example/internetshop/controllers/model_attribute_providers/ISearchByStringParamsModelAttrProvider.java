package com.example.internetshop.controllers.model_attribute_providers;

import com.example.internetshop.models.ProductSearchByStringParams;
import com.example.internetshop.models.ProductSearchFilters;
import com.example.internetshop.repositories.ProductRepository;
import org.springframework.data.util.Pair;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * <p>
 * Provides product search by string params to the model (has default implementation).
 * Should be implemented by Controller.
 * </p>
 * <p>
 * * this attribute is required in the main_layout, so all controllers need to implement this interface.
 * </p>
 * */
public interface ISearchByStringParamsModelAttrProvider {
    @ModelAttribute
    default void setSearchByStringParamsAttr(Model model) {
        model.addAttribute("searchByStringParams", new ProductSearchByStringParams());
    }
}
