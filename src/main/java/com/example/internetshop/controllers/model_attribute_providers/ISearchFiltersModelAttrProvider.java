package com.example.internetshop.controllers.model_attribute_providers;

import com.example.internetshop.models.ProductSearchByStringParams;
import com.example.internetshop.models.ProductSearchFilters;
import com.example.internetshop.repositories.ProductRepository;
import org.springframework.data.util.Pair;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * <p>
 * Provides product search filters to the model (has default implementation).
 * Should be implemented by Controller.
 * </p>
 * <p>
 * Required in controllers that return views with search filters sidebar.
 * </p>
 * */
public interface ISearchFiltersModelAttrProvider {
    /*A hack to get injected services in the interface to use in methods with default implementation*/
    ProductRepository getProductRepository();

    @ModelAttribute
    default void setSearchFiltersAttr(Model model) {
        var searchFilters = new ProductSearchFilters();
        var priceRange = getPriceRange();
        searchFilters.setMinPrice(priceRange.getFirst());
        searchFilters.setMaxPrice(priceRange.getSecond());
        searchFilters.setPriceRangeMin(priceRange.getFirst());
        searchFilters.setPriceRangeMax(priceRange.getSecond());

        model.addAttribute("searchByStringParams", new ProductSearchByStringParams());
        model.addAttribute("searchFilters", searchFilters);
        model.addAttribute("globalMinPrice", priceRange.getFirst());
        model.addAttribute("globalMaxPrice", priceRange.getSecond());
    }

    private Pair<Double, Double> getPriceRange() {
        double minPrice = 0;
        double maxPrice = 100;

        var productRepository = getProductRepository();
        var minPriceProduct = productRepository.getProductWithMinPrice();
        var maxPriceProduct = productRepository.getProductWithMaxPrice();
        if (minPriceProduct.isPresent()) {
            minPrice = minPriceProduct.get().getPrice();
        }
        if (maxPriceProduct.isPresent()) {
            maxPrice = maxPriceProduct.get().getPrice();
        }
        return Pair.of(minPrice, maxPrice);
    }
}
