package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.controllers.model_attribute_providers.ISearchFiltersModelAttrProvider;
import com.example.internetshop.models.ProductSearchByStringParams;
import com.example.internetshop.models.ProductSearchFilters;
import com.example.internetshop.repositories.ProductCategoryRepository;
import com.example.internetshop.repositories.ProductRepository;
import com.example.internetshop.services.IAuthenticationService;
import com.example.internetshop.services.IProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/search")
public class SearchController implements IBasicModelAttrProvider, ISearchFiltersModelAttrProvider {
    private final IAuthenticationService authService;
    private final ProductRepository productRepository;
    private final IProductCategoryService categoryService;

    @Autowired
    public SearchController(
            IAuthenticationService authService,
            ProductRepository productRepository,
            IProductCategoryService categoryService)
    {
        this.authService = authService;
        this.productRepository = productRepository;
        this.categoryService = categoryService;
    }

    @GetMapping(params = {"q"})
    public String searchByString(
            @RequestParam("q") String q,
            @ModelAttribute(name = "searchByStringParams") ProductSearchByStringParams searchParams,
            Model model) {
        if (!searchParams.isSearchInName() && !searchParams.isSearchInBrand() && !searchParams.isSearchInCategory()) {
            searchParams.setSearchInName(true);
        }
        var products = productRepository.findByContainedString(q, searchParams.isSearchInName(),
                searchParams.isSearchInBrand(), searchParams.isSearchInCategory());
        model.addAttribute("products", products);
        return "index";
    }

    @PostMapping()
    public String searchFilter(
            @ModelAttribute(name = "searchFilters") ProductSearchFilters searchFilters,
            Model model
    ) {
        var products = productRepository.findBySearchFilters(searchFilters);
        var filterCategoryNames = categoryService.findAll().stream()
                .filter(cat -> searchFilters.getCategories().contains(cat.getId()))
                .map(cat -> cat.getName())
                .collect(Collectors.toList());
        model.addAttribute("products", products);
        model.addAttribute("filterCategoryNames", filterCategoryNames);
        return "index";
    }

    @ModelAttribute
    public void getBrandsAndCategories(Model model) {
        var categories = categoryService.getAllOrderedByName();
        var brands = productRepository.findDistinctBrandNames();
        model.addAttribute("productCategories", categories);
        model.addAttribute("productBrands", brands);
    }


    @Override
    public ProductRepository getProductRepository() {
        return productRepository;
    }

    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
