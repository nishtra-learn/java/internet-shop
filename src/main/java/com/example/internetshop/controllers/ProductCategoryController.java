package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.models.ErrorMap;
import com.example.internetshop.models.domain.ProductCategory;
import com.example.internetshop.services.IAuthenticationService;
import com.example.internetshop.services.IProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@Secured("ROLE_ADMIN")
@RequestMapping("/categories")
public class ProductCategoryController implements IBasicModelAttrProvider {
    private final IAuthenticationService authService;
    private final IProductCategoryService categoryService;

    @Autowired
    public ProductCategoryController(
            IAuthenticationService authService,
            IProductCategoryService categoryService)
    {
        this.authService = authService;
        this.categoryService = categoryService;
    }

    @GetMapping("")
    public String index(Model model) {
        var categories = categoryService.findAll();
        model.addAttribute("categories", categories);
        return "admin/categories";
    }

    @GetMapping("new")
    public String create(Model model) {
        var category = new ProductCategory();
        model.addAttribute("category", category);
        model.addAttribute("action", "create");
        return "admin/category_form";
    }

    @PostMapping("new")
    public String createConfirm(
            @ModelAttribute ProductCategory category,
            BindingResult result,
            Model model)
    {
        if (result.hasErrors()) {
            var errors = ErrorMap.fromBindingResult(result);
            model.addAttribute("errors", errors);
            model.addAttribute("action", "create");
            return "admin/category_form";
        }

        categoryService.save(category);
        return "redirect:/categories";
    }

    @GetMapping("update")
    public String update(
            @RequestParam Long id,
            Model model)
    {
        var category = categoryService.findById(id);

        if (category.isEmpty()) {
            return "redirect:/categories";
        }

        model.addAttribute("category", category.get());
        model.addAttribute("action", "update");
        return "admin/category_form";
    }

    @PostMapping("update")
    public String updateConfirm(@ModelAttribute ProductCategory category,
            BindingResult result,
            Model model)
    {
        if (result.hasErrors()) {
            var errors = ErrorMap.fromBindingResult(result);
            model.addAttribute("errors", errors);
            model.addAttribute("action", "update");
            return "admin/category_form";
        }

        categoryService.save(category);
        return "redirect:/categories";
    }

    @PostMapping("delete")
    public String deleteConfirm(@RequestParam Long id) {
        var category = categoryService.findById(id);
        if (category.isEmpty())
            return "redirect:/categories";

        for (var item : category.get().getProducts()) {
            item.setCategory(null);
        }
        categoryService.deleteById(id);
        return "redirect:/categories";
    }


    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
