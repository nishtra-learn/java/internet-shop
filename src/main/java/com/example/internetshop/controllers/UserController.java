package com.example.internetshop.controllers;

import com.example.internetshop.controllers.model_attribute_providers.IBasicModelAttrProvider;
import com.example.internetshop.models.ErrorMap;
import com.example.internetshop.models.domain.User;
import com.example.internetshop.repositories.UserRepository;
import com.example.internetshop.services.AuthenticationService;
import com.example.internetshop.services.IAuthenticationService;
import com.example.internetshop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.EntityNotFoundException;
import javax.security.sasl.AuthenticationException;
import javax.validation.Valid;

@Controller
@RequestMapping(path = {"user", "profile"})
@Secured({"ROLE_USER", "ROLE_ADMIN"})
public class UserController implements IBasicModelAttrProvider {
    private final AuthenticationService authService;
    private final UserService userService;
    private final UserRepository userRepository;
    private ErrorMap errors;

    @Autowired
    public UserController(
            AuthenticationService authenticationService,
            UserService userService,
            UserRepository userRepository) {
        this.authService = authenticationService;
        this.userService = userService;
        this.userRepository = userRepository;
    }


    @GetMapping
    public String profilePage(Model model) {
        var user = authService.getAuthenticatedUserFromDatabase();
        if (user == null) {
            throw new UsernameNotFoundException("There is no user registered with this username");
        }
        model.addAttribute("user", user);
        return "profile_page";
    }

    @GetMapping(params = {"id"})
    public String adminProfilePage(@RequestParam Long id, Model model) {
        var user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new EntityNotFoundException("There is no user with this id");
        }
        model.addAttribute("user", user.get());
        return "admin/profile_page";
    }

    @GetMapping("list")
    public String userList(Model model) {
        var users = userRepository.findAll();
        model.addAttribute("users", users);
        return "admin/users";
    }

    @PostMapping("update")
    public String updateProfile(
            @Valid @ModelAttribute User user,
            BindingResult bindResult,
            @RequestParam(required = false, defaultValue = "false") Boolean isAdminPage,
            Model model,
            RedirectAttributes redirectAttributes)
            throws AuthenticationException
    {
        if (bindResult.hasErrors()) {
            errors = ErrorMap.fromBindingResult(bindResult);
            model.addAttribute("errors", errors);
            return isAdminPage ? "admin/profile_page" : "profile_page";
        }

        // TODO user validation

        userService.updateUserWithAuthorizationCheck(user);
        redirectAttributes.addFlashAttribute("success", "Profile updated");
        return isAdminPage ? "redirect:/user?id="+user.getId() : "redirect:/profile";
    }

    @PostMapping("update-password")
    public String updateUserPassword(
            @RequestParam("id") Long userId,
            @RequestParam("password") String password,
            @RequestParam("passwordConfirm") String passwordConfirm,
            @RequestParam(required = false, defaultValue = "false") Boolean isAdminPage,
            Model model,
            RedirectAttributes redirectAttributes)
            throws AuthenticationException
    {
        // TODO validate password

        if (!password.equals(passwordConfirm)) {
            errors.addError("passwordConfirm", "Passwords do not match!");
            var user = userRepository.findById(userId).orElse(null);
            model.addAttribute("errors", errors);
            model.addAttribute("user", user);
            return isAdminPage ? "admin/profile_page" : "profile_page";
        }

        userService.updateUserPasswordWithAuthorizationCheck(userId, password);
        redirectAttributes.addFlashAttribute("success", "New password set");

        return isAdminPage ? "redirect:/user?id="+userId : "redirect:/profile";
    }

    @PostMapping ("delete")
    String deleteConfirm(@RequestParam Long id) {
        userRepository.deleteById(id);
        return "redirect:/user/list";
    }

    @PostMapping("ban")
    public String banUser(@RequestParam Long id) {
        userService.banUser(id);
        return "redirect:/user/list";
    }

    @PostMapping("restore")
    public String unbanUser(@RequestParam Long id) {
        userService.unbanUser(id);
        return "redirect:/user/list";
    }

    @PostMapping("give-admin")
    public String giveAdminRights(@RequestParam Long id) {
        userService.giveAdminRights(id);
        return "redirect:/user/list";
    }

    @PostMapping("revoke-admin")
    public String revokeAdminRights(@RequestParam Long id) {
        userService.revokeAdminRights(id);
        return "redirect:/user/list";
    }

    @ModelAttribute
    public void addErrorMap(Model model) {
        errors = new ErrorMap();
        model.addAttribute("errors", errors);
    }


    @Override
    public IAuthenticationService getAuthService() {
        return authService;
    }
}
