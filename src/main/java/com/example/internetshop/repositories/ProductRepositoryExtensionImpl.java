package com.example.internetshop.repositories;

import com.example.internetshop.models.domain.Product;
import com.example.internetshop.models.ProductSearchFilters;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepositoryExtensionImpl implements ProductRepositoryExtension {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Product> findBySearchFilters(ProductSearchFilters searchFilters) {
        var cb = entityManager.getCriteriaBuilder();
        var query = cb.createQuery(Product.class);
        var product = query.from(Product.class);

        var predicates = new ArrayList<Predicate>();

        // clause on brand
        Path<String> brandPath = product.get("brandName");
        if (searchFilters.getBrands().size() > 0) {
            CriteriaBuilder.In<String> inBrandsClause = cb.in(brandPath);
            for (var brand : searchFilters.getBrands()) {
                inBrandsClause.value(brand);
            }
            predicates.add(inBrandsClause);
        }

        // clause on category
        Path<Long> categoryIdPath = product.get("category").get("id");
        if (searchFilters.getCategories().size() > 0) {
            CriteriaBuilder.In<Long> inCategoriesClause = cb.in(categoryIdPath);
            for (var id : searchFilters.getCategories()) {
                inCategoriesClause.value(id);
            }
            predicates.add(inCategoriesClause);
        }

        // clause oon price
        Path<Double> pricePath = product.get("price");
        predicates.add(cb.ge(pricePath, searchFilters.getMinPrice()));
        predicates.add(cb.le(pricePath, searchFilters.getMaxPrice()));

        query.select(product)
                .where(cb.and(predicates.toArray(new Predicate[0])));

        return entityManager.createQuery(query)
                .getResultList();
    }
}
