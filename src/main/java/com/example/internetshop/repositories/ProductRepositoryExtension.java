package com.example.internetshop.repositories;

import com.example.internetshop.models.domain.Product;
import com.example.internetshop.models.ProductSearchFilters;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepositoryExtension {
    /**
     * Find products matching the provided search filters. Filters referencing different fields have 'AND' relations
     * */
    List<Product> findBySearchFilters(ProductSearchFilters searchFilters);
}
