package com.example.internetshop.repositories;

import com.example.internetshop.models.domain.Product;
import com.example.internetshop.models.projection.ProductPopularity;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, ProductRepositoryExtension {
    /**
     * Select all products sorted by the number of items ordered
     * */
    @Query("SELECT new com.example.internetshop.models.projection.ProductPopularity(p.id, p.name, SUM(oi.quantity)) " +
            "FROM Product AS p JOIN p.orderItems AS oi " +
            "GROUP BY p.id ORDER BY SUM(oi.quantity) DESC")
    List<ProductPopularity> findProductOrderByPopularity(Pageable pageable);

    default List<ProductPopularity> findTop100ProductOrderByPopularity() {
        return findProductOrderByPopularity(PageRequest.of(0, 100));
    }

    /**
     * Find products that have [searchKey] in their name
     * */
    List<Product> findByNameContainingIgnoreCase(String searchKey);

    /**
     * Select unique brands
     * */
    @Query("select distinct p.brandName from Product as p " +
            "where p.brandName is not null order by p.brandName")
    List<String> findDistinctBrandNames();

    /**
     * Find products containing [searchKey] either in name, brand or category name
     * */
    @Query("select p from Product as p where (:inName = true or :inBrand = true or :inCategory = true) " +
            "and (" +
            "(:inName = true and lower(p.name) like lower(concat('%', :q, '%'))) " +
            "or (:inBrand = true and lower(p.brandName) like lower(concat('%', :q, '%'))) " +
            "or (:inCategory = true and lower(p.category.name) like lower(concat('%', :q, '%'))))")
    List<Product> findByContainedString(
            @Param("q") String searchKey,
            @Param("inName") boolean inName,
            @Param("inBrand") boolean inBrand,
            @Param("inCategory") boolean inCategoryName);


    @Query("select p from Product as p " +
            "where p.brandName in :brands " +
            "or p.category.id in :categories")
    List<Product> findByBrandOrCategory(
            @Param("brands") List<String> brandList,
            @Param("categories") List<Long> categoryIdList);

    default Optional<Product> getProductWithMinPrice() {
        var productPage = findAll(PageRequest.of(0, 1, Sort.by("price").ascending()));
        return productPage.stream().findFirst();
    }

    default Optional<Product> getProductWithMaxPrice() {
        var productPage = findAll(PageRequest.of(0, 1, Sort.by("price").descending()));
        return productPage.stream().findFirst();
    }


    /**
     * Execute "soft delete"
     * */
    default void deleteById(Long id) {
        var product = findById(id);
        if (product.isPresent()) {
            product.get().setDeleted(1);
            save(product.get());
        }
    }
}
