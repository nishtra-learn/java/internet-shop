package com.example.internetshop.repositories;

import com.example.internetshop.models.domain.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {
    @Query("select cat from ProductCategory as cat order by cat.name")
    List<ProductCategory> getAllOrderedByName();
}
