package com.example.internetshop.repositories;

import com.example.internetshop.models.domain.User;
import com.example.internetshop.models.projection.UserMoneySpent;
import com.example.internetshop.models.projection.UserOrdersMade;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByUsername(String login);

    @Query("SELECT new com.example.internetshop.models.projection.UserOrdersMade(u.username, COUNT(o.id)) " +
            "FROM Order AS o JOIN o.user AS u " +
            "GROUP BY u.id ORDER BY COUNT(o.id) DESC")
    List<UserOrdersMade> findAllUsersOrderByOrdersMade(Pageable pageable);

    default List<UserOrdersMade> findTop100UsersOrderByOrdersMade() {
        return findAllUsersOrderByOrdersMade(PageRequest.of(0, 100));
    }

    @Query("SELECT new com.example.internetshop.models.projection.UserMoneySpent(u.username, SUM(oi.quantity * oi.product.price)) " +
            "FROM Order AS o JOIN o.user AS u JOIN o.items AS oi " +
            "GROUP BY u.id ORDER BY SUM(oi.quantity * oi.product.price) DESC")
    List<UserMoneySpent> findAllUsersOrderByMoneySpent(Pageable pageable);

    default List<UserMoneySpent> findTop100UsersOrderByMoneySpent() {
        return findAllUsersOrderByMoneySpent(PageRequest.of(0, 100));
    }
}
