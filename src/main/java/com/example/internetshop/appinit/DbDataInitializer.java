package com.example.internetshop.appinit;

import com.example.internetshop.models.domain.*;
import com.example.internetshop.repositories.OrderRepository;
import com.example.internetshop.repositories.ProductCategoryRepository;
import com.example.internetshop.repositories.ProductRepository;
import com.example.internetshop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class DbDataInitializer implements ApplicationRunner {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;
    private final ProductCategoryRepository categoryRepository;

    @Autowired
    public DbDataInitializer(
            UserRepository userRepository,
            ProductRepository productRepository,
            OrderRepository orderRepository,
            ProductCategoryRepository categoryRepository)
    {
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.categoryRepository = categoryRepository;
    }


    @Override
    public void run(ApplicationArguments args)
            throws Exception
    {
        initUserData();
        initProductData();
        initOrderData();

        System.out.println(userRepository.count() + " users inserted");
        System.out.println(productRepository.count() + " products inserted");
        System.out.println(orderRepository.count() + " orders inserted");
    }

    private void initUserData() {
        final String roleUser = "USER";
        final String roleAdmin = "ADMIN";

        var admin = new User("admin", "admin", roleAdmin);
        var user = new User("user", "user", roleUser);
        var banned = new User("banned", "1111", roleUser);
        banned.setActive(false);
        userRepository.save(admin);
        userRepository.save(user);
        userRepository.save(banned);

        var users = new ArrayList<User>();
        users.add(new User("John Doe", "1111", roleUser));
        users.add(new User("oduvan", "1111", roleUser));
        users.add(new User("Romashka", "1111", roleUser));
        users.add(new User("Marusya", "1111", roleUser));
        users.add(new User("Steven Spielberb", "1111", roleUser));
        for (var u : users) {
            u.setAddress("Address");
            u.setPhoneNumber("+380657645245");
            u.setFirstName("Cool First Name");
            u.setLastName("Cool Last Name");
            u.setEmail("my_mail@mail.com");
        }
        userRepository.saveAll(users);
    }

    private void initProductData() {
        var catFood = new ProductCategory("Food");
        var catFurniture = new ProductCategory("Furniture");
        var catComputerParts = new ProductCategory("Computer Parts");
        var catConsoles = new ProductCategory("Consoles");
        var catElectricAppliances = new ProductCategory("Electric Appliances");
        var categories = new ArrayList<ProductCategory>();
        categories.add(catFood);
        categories.add(catComputerParts);
        categories.add(catFurniture);
        categories.add(catConsoles);
        categories.add(catElectricAppliances);
        categories.add(new ProductCategory("Alcohol"));
        categories.add(new ProductCategory("Personal Hygiene"));
        categories.add(new ProductCategory("Chemical"));
        categoryRepository.saveAll(categories);


        var products = new ArrayList<Product>();
        products.add(new Product("Xbox 360", 100d, "Xbox", catConsoles));
        products.add(new Product("New TV", 200d, "Samsung", catElectricAppliances));
        products.add(new Product("Sofa", 500d, "Dnipro Mebli", catFurniture));
        products.add(new Product("Doughnut", 2.5d, null, catFood));
        products.add(new Product("Pizza", 12.5d, "Pizza Hut", catFood));
        products.add(new Product("Brand Ice Cream", 5.5d, "Pizza Hut", catFood));

        // generated values
        var brands = getBrandList();
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            double price = (random.nextDouble() + 0.1) * 500;
            price = (int) (price * 100) / 100d; // truncate
            // add brand to about 70% of products
            var brand = random.nextDouble() < 0.7
                    ? brands.get(random.nextInt(brands.size()))
                    : null;
            // add category to about 70% of products
            var category = random.nextDouble() < 0.7
                    ? categories.get(random.nextInt(categories.size()))
                    : null;
            products.add(new Product("Product " + (i + 1), price, brand, category));
        }

        productRepository.saveAll(products);
    }

    private void initOrderData() {
        var users = userRepository.findAll();
        var products = productRepository.findAll();
        var orders = new ArrayList<Order>();

        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            var randomUser = users.get(random.nextInt(users.size()));
            var order = new Order(randomUser);
            var itemsToOrder = random.nextInt(5) + 1;
            for (int j = 0; j < itemsToOrder; j++) {
                var randomProduct = products.get(random.nextInt(products.size()));
                var quantity = random.nextInt(2) + 1;
                order.getItems().add(new OrderItem(order, randomProduct, quantity));
            }
            orders.add(order);
        }
        orderRepository.saveAll(orders);
    }

    private List<String> getBrandList() {
        var brands = new ArrayList<String>();
        brands.add("Samsung");
        brands.add("Skechers");
        brands.add("Asus");
        brands.add("Microsoft");
        brands.add("Toyota");
        brands.add("Adidas");
        brands.add("Puma");
        brands.add("Pizza Hut");
        brands.add("Domino");
        return brands;
    }
}
