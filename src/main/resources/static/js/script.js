document.addEventListener("DOMContentLoaded", e => {
    priceSliderConfiguration();
})

function priceSliderConfiguration() {
    slider = document.getElementById("price-slider");

    if (slider == null)
        return;

    let minPriceSubmitEl = document.getElementById("min-price");
    let maxPriceSubmitEl = document.getElementById("max-price");
    let minPriceInputEl = document.getElementById("min-price-input");
    let maxPriceInputEl = document.getElementById("max-price-input");

    let minPrice = minPriceSubmitEl.value;
    let maxPrice = maxPriceSubmitEl.value;
    let globalMinPrice = slider.getAttribute("min");
    let globalMaxPrice = slider.getAttribute("max");

    noUiSlider.create(slider, {
        start: [minPrice, maxPrice],
        connect: true,
        range: {
            'min': Math.floor(globalMinPrice),
            'max': Math.ceil(globalMaxPrice)
        },
        step: 1,
        tooltips: [true, true]
    });

    slider.noUiSlider.on("update", (values) => {
        minPriceSubmitEl.value = values[0];
        minPriceInputEl.value = values[0];
        maxPriceSubmitEl.value = values[1];
        maxPriceInputEl.value = values[1];
    });

    minPriceInputEl.addEventListener("keydown", (e) => {
        if (e.key === "Enter" || e.code === "Enter") {
            e.preventDefault();
            e.stopPropagation();
            let newVal = minPriceInputEl.value;
            let sliderRangeMin = slider.noUiSlider.options.range.min;
            let sliderMinVal = slider.noUiSlider.get()[0];
            let sliderMaxVal = slider.noUiSlider.get()[1];

            if (isNaN(newVal)) {
                newVal = sliderMinVal;
            }
            else {
                if (newVal < sliderRangeMin) newVal = sliderRangeMin;
                else if (newVal > sliderMaxVal) newVal = sliderMaxVal;
            }

            slider.noUiSlider.setHandle(0, newVal);
        }
    });

    maxPriceInputEl.addEventListener("keydown", (e) => {
        if (e.key === "Enter" || e.code === "Enter") {
            e.preventDefault();
            e.stopPropagation();
            let newVal = maxPriceInputEl.value;
            let sliderRangeMax = slider.noUiSlider.options.range.max;
            let sliderMinVal = slider.noUiSlider.get()[0];
            let sliderMaxVal = slider.noUiSlider.get()[1];

            if (isNaN(newVal)) {
                newVal = sliderMaxVal;
            }
            else {
                if (newVal > sliderRangeMax) newVal = sliderRangeMax;
                else if (newVal < sliderMinVal) newVal = sliderMinVal;
            }

            slider.noUiSlider.setHandle(1, newVal);
        }
    });
}